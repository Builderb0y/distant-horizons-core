/*
 *    This file is part of the Distant Horizons mod
 *    licensed under the GNU LGPL v3 License.
 *
 *    Copyright (C) 2020-2023 James Seibel
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, version 3.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.seibel.distanthorizons.core.util.threading;

import com.seibel.distanthorizons.core.config.Config;
import com.seibel.distanthorizons.core.config.listeners.ConfigChangeListener;
import com.seibel.distanthorizons.core.util.ThreadUtil;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Holds each thread pool the system uses.
 * 
 * @see ThreadUtil
 */
public class ThreadPoolUtil
{
	//=========================//
	// standalone thread pools //
	//=========================//
	
	// standalone thread pools all handle independent systems
	// and don't interfere with any other pool
	
	public static final DhThreadFactory FILE_HANDLER_THREAD_FACTORY = new DhThreadFactory("File Handler", Thread.MIN_PRIORITY);
	private static ConfigThreadPool fileHandlerThreadPool;
	@Nullable
	public static ThreadPoolExecutor getFileHandlerExecutor() { return fileHandlerThreadPool.executor; }
	
	public static final DhThreadFactory UPDATE_PROPAGATOR_THREAD_FACTORY = new DhThreadFactory("LOD Update Propagator", Thread.MIN_PRIORITY);
	private static ConfigThreadPool updatePropagatorThreadPool;
	@Nullable
	public static ThreadPoolExecutor getUpdatePropagatorExecutor() { return updatePropagatorThreadPool.executor; }
	
	public static final DhThreadFactory WORLD_GEN_THREAD_FACTORY = new DhThreadFactory("World Gen", Thread.MIN_PRIORITY);
	private static ConfigThreadPool worldGenThreadPool;
	@Nullable
	public static ThreadPoolExecutor getWorldGenExecutor() { return worldGenThreadPool.executor; }
	
	public static final String CLEANUP_THREAD_NAME = "Cleanup";
	private static ThreadPoolExecutor cleanupThreadPool;
	@Nullable
	public static ThreadPoolExecutor getCleanupExecutor() { return cleanupThreadPool; }
	
	public static final String BEACON_CULLING_THREAD_NAME = "Beacon Culling";
	private static ThreadPoolExecutor beaconCullingThreadPool;
	@Nullable
	public static ThreadPoolExecutor getBeaconCullingExecutor() { return beaconCullingThreadPool; }
	
	public static final DhThreadFactory NETWORK_COMPRESSION_THREAD_FACTORY = new DhThreadFactory("Network Compression", Thread.MIN_PRIORITY);
	private static ConfigThreadPool networkCompressionThreadPool;
	@Nullable
	public static ThreadPoolExecutor getNetworkCompressionExecutor() { return networkCompressionThreadPool.executor; }
	
	
	
	//======================//
	// worker threads pools //
	//======================//
	
	// worker thread pools are generally related with LOD building
	// and all share an underlying number of threads.
	// WARNING: great care should be used when setting up these threads since deadlock can occur if they are handled poorly.
	
	public static final DhThreadFactory CHUNK_TO_LOD_BUILDER_THREAD_FACTORY = new DhThreadFactory("LOD Builder - Chunk to Lod Builder", Thread.MIN_PRIORITY);
	private static ConfigThreadPool chunkToLodBuilderThreadPool;
	@Nullable
	public static ThreadPoolExecutor getChunkToLodBuilderExecutor() { return (chunkToLodBuilderThreadPool != null) ? chunkToLodBuilderThreadPool.executor : null; }
	
	public static final DhThreadFactory BUFFER_BUILDER_THREAD_FACTORY = new DhThreadFactory("LOD Builder - Buffer Builder", Thread.MIN_PRIORITY);
	private static ConfigThreadPool bufferBuilderThreadPool;
	@Nullable
	public static ThreadPoolExecutor getBufferBuilderExecutor() { return (bufferBuilderThreadPool != null) ? bufferBuilderThreadPool.executor : null; }
	
	
	/** how many total worker threads can be used */
	private static int workerThreadSemaphoreCount = Config.Common.MultiThreading.numberOfLodBuilderThreads.get();
	public static int getWorkerThreadCount() { return workerThreadSemaphoreCount; }
	
	private static Semaphore workerThreadSemaphore = null;
	private static ConfigChangeListener<Integer> workerThreadSemaphoreConfigListener = null;
	
	
	
	//=================//
	// setup / cleanup //
	//=================//
	
	public static void setupThreadPools()
	{
		// standalone threads //
		
		fileHandlerThreadPool = new ConfigThreadPool(FILE_HANDLER_THREAD_FACTORY, Config.Common.MultiThreading.numberOfFileHandlerThreads, Config.Common.MultiThreading.runTimeRatioForFileHandlerThreads, null);
		updatePropagatorThreadPool = new ConfigThreadPool(UPDATE_PROPAGATOR_THREAD_FACTORY, Config.Common.MultiThreading.numberOfUpdatePropagatorThreads, Config.Common.MultiThreading.runTimeRatioForUpdatePropagatorThreads, null);
		worldGenThreadPool = new ConfigThreadPool(WORLD_GEN_THREAD_FACTORY, Config.Common.MultiThreading.numberOfWorldGenerationThreads, Config.Common.MultiThreading.runTimeRatioForWorldGenerationThreads, null);
		networkCompressionThreadPool = new ConfigThreadPool(NETWORK_COMPRESSION_THREAD_FACTORY, Config.Common.MultiThreading.numberOfNetworkCompressionThreads, Config.Common.MultiThreading.runTimeRatioForNetworkCompressionThreads, null);
		cleanupThreadPool = ThreadUtil.makeSingleThreadPool(CLEANUP_THREAD_NAME);
		beaconCullingThreadPool = ThreadUtil.makeSingleThreadPool(BEACON_CULLING_THREAD_NAME);
		
		
		
		// worker threads //
		
		// create thread semaphore
		if (Config.Common.MultiThreading.enableLodBuilderThreadLimiting.get())
		{
			workerThreadSemaphoreCount = Config.Common.MultiThreading.numberOfLodBuilderThreads.get();
			workerThreadSemaphore = new Semaphore(workerThreadSemaphoreCount);
			
			workerThreadSemaphoreConfigListener = new ConfigChangeListener<>(Config.Common.MultiThreading.numberOfLodBuilderThreads, (val) ->
			{
				int changePermit = val - workerThreadSemaphoreCount;
				if (changePermit > 0)
				{
					workerThreadSemaphore.release(changePermit);
				}
				else
				{
					workerThreadSemaphore.acquireUninterruptibly(changePermit * -1);
				}
				workerThreadSemaphoreCount = workerThreadSemaphoreCount + changePermit;
			});
		}
		
		// create thread pools
		chunkToLodBuilderThreadPool = new ConfigThreadPool(CHUNK_TO_LOD_BUILDER_THREAD_FACTORY, Config.Common.MultiThreading.numberOfLodBuilderThreads, Config.Common.MultiThreading.runTimeRatioForLodBuilderThreads, workerThreadSemaphore);
		bufferBuilderThreadPool = new ConfigThreadPool(BUFFER_BUILDER_THREAD_FACTORY, Config.Common.MultiThreading.numberOfLodBuilderThreads, Config.Common.MultiThreading.runTimeRatioForLodBuilderThreads, workerThreadSemaphore);
		
	}
	
	public static void shutdownThreadPools()
	{
		// standalone threads
		fileHandlerThreadPool.shutdownExecutorService();
		updatePropagatorThreadPool.shutdownExecutorService();
		worldGenThreadPool.shutdownExecutorService();
		networkCompressionThreadPool.shutdownExecutorService();
		cleanupThreadPool.shutdown();
		beaconCullingThreadPool.shutdown();
		
		
		// worker threads
		ThreadPoolUtil.chunkToLodBuilderThreadPool.shutdownExecutorService();
		ThreadPoolUtil.bufferBuilderThreadPool.shutdownExecutorService();
		
		workerThreadSemaphore = null;
		
		if (workerThreadSemaphoreConfigListener != null)
		{
			workerThreadSemaphoreConfigListener.close();
			workerThreadSemaphoreConfigListener = null;
		}
	}
	
}
